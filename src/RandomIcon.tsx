import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library, IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faRandom } from '@fortawesome/free-solid-svg-icons/faRandom';
import * as solidIcons from '@fortawesome/free-solid-svg-icons';
import * as reactIcons from '@fortawesome/react-fontawesome';
import * as coreIcons from '@fortawesome/fontawesome-svg-core';

const allIcons = { ...solidIcons, ...reactIcons, ...coreIcons };
library.add(faRandom);

const RandomIcon = () => {
  const [randomIcon, setRandomIcon] = useState<IconDefinition | null>(null);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const handleClick = () => {
    setIsLoading(true);
    setTimeout(() => {
      const iconsArray = Object.values(allIcons);
      const randomIndex = Math.floor(Math.random() * iconsArray.length);
      setRandomIcon(iconsArray[randomIndex] as IconDefinition);
      setIsLoading(false);
    }, 3000);
  };

  return (
      <>
        <button onClick={handleClick} disabled={isLoading} >
            {isLoading ? 'Loading...' : 'Show Random Icon'}
        </button>
        {randomIcon && <FontAwesomeIcon icon={randomIcon} size="3x" className='svg' />}
    </>
  );
};

export default RandomIcon;
