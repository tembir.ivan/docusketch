import React from 'react';
import './App.css';
import RandomIcon from './RandomIcon';

function App() {
  return (
    <div className='wrapper'>
      <RandomIcon />
    </div>
  );
}

export default App;
